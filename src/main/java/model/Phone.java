package model;

import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
//@RequiredArgsConstructor
@Embeddable
public class Phone {

    @NonNull
    private String number;

}
