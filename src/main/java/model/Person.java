package model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "isik")
public class Person extends BaseEntity {

    @NonNull
    @Column(name = "nimi")
    private String name;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "aadressi_id")
    private Address address;

//    @OneToMany(fetch = FetchType.EAGER,
//            cascade = CascadeType.ALL)

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "isiku_telefonid",
            joinColumns = @JoinColumn(name = "isiku_id", referencedColumnName = "id"))
    private List<Phone> phones = new ArrayList<>();
}
