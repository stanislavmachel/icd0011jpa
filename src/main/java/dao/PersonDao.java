package dao;

import model.Person;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Objects;

@Repository
public class PersonDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(Person person) {
        System.out.println(em);

        if (Objects.isNull(person.getId())) {
            em.persist(person);
        } else {
            em.merge(person);
        }
    }

    public List<Person> findAll() {
        return em.createQuery("select p from Person p").getResultList();
    }


    public Person findPersonByName(String name) {
        var query = em.createQuery("select p from Person p where p.name = :name", Person.class);
        query.setParameter("name", name);
        return query.getSingleResult();
    }
}
