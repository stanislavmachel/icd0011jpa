package main;

import config.DbConfig;
import config.HsqlDataSource;
import config.PostgresDataSource;
import dao.PersonDao;
import model.Address;
import model.Person;
import model.Phone;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Tester {

    public static void main(String[] args) {

        ConfigurableApplicationContext ctx =
              new AnnotationConfigApplicationContext(
                      DbConfig.class, HsqlDataSource.class);

        try (ctx) {

            PersonDao dao = ctx.getBean(PersonDao.class);


//            var person = new Person("Bob");
//
//            var address = new Address("X street");
//            person.setAddress(address);
//
//            dao.save(person);



//            System.out.println(dao.findAll());


//            var bob = dao.findPersonByName("Bob");
//            bob.setName("test");

//            dao.save(bob);

            System.out.println(dao.findAll());

            var bob = new Person("Bob");

            bob.getPhones().add(new Phone("123"));
            bob.getPhones().add(new Phone("345"));

            dao.save(bob);
//
//            System.out.println(bob);
        }




    }
}